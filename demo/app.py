from aiohttp import web
from .routes import setup_routes
from motor.motor_asyncio import AsyncIOMotorClient


async def create_app(config: dict) -> web.Application:
    app = web.Application()
    app['config'] = config
    app['db'] = AsyncIOMotorClient().shop
    setup_routes(app)
    return app
