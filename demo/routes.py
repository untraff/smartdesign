from demo import handlers
from aiohttp import web


def setup_routes(app):
    app.router.add_routes([
        web.get('/product/get_by_id/{id}/', handlers.product_get),
        web.post('/product/filter/', handlers.ProductsSearch.post),
        web.post('/product/create/', handlers.ProductCreate.post),
    ])


