from bson import ObjectId
from motor.motor_asyncio import AsyncIOMotorDatabase


class Product:

    collection = None

    def __init__(self):
        pass

    @staticmethod
    async def get_one(db: AsyncIOMotorDatabase, product_id: str):

        try:
            product = await db.products.find({"_id": ObjectId(product_id)}).to_list(20)
        except Exception as e:
            product = []

        return product

    @staticmethod
    async def find_all(db: AsyncIOMotorDatabase, data):
        query = {}
        if 'name' in data:
            query['name'] = data['name']
        # TODO: It must be a better way to find one subparam, but i dont know
        if 'params' in data:
            for k, v in data['params'].items():
                query[f'params.{k}'] = v
        
        if not query:
            return []

        try:
            products = await db.products.find(query).to_list(20)
            print(products)
        except Exception as e:
            products = []

        return products

    @staticmethod
    async def create_new(db: AsyncIOMotorDatabase, name: str, params: dict, data={}):

        data['name'] = name
        data['params'] = params

        try:
            db.products.insert_one(data)
        except Exception as e:
            print(e)

