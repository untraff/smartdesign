from aiohttp import web
from bson import ObjectId
from demo import constants
from demo.models import Product
import json


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


async def product_get(request):
    """get product by id
    """
    db = request.app['db']
    id = request.match_info['id']
    products = await Product.get_one(db, id)

    json_resp = JSONEncoder().encode(products)
    return web.Response(text=f'{json_resp}')


class ProductsSearch(web.View):

    async def post(self):
        data = await self.json()
        
        products = await Product.find_all(self.app['db'], data)
        json_resp = JSONEncoder().encode(products)
        
        return web.Response(text=f'{json_resp}')


class ProductCreate(web.View):

    async def post(self):
        try:
            data = await self.json()
        except json.decoder.JSONDecodeError:
            return web.Response(text=constants.JSON_DATA_ERROR)
        try:
            name = data['name']
            params = data['params']
        except KeyError:
            return web.Response(text=constants.KEY_ERROR)

        tmp = await Product.create_new(self.app['db'], name=name, params=params)

        return web.Response(text='created')
