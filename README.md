# Установка проекта


## Настройка БД

Можно установить сервер БД, либо использовать docker:
```docker run -d -p 27017:27017 mongo```


## Установка Python пакетов

Активировав вирутальное окружение, запустите команду
```pip install -r requirements.txt```


## Запуск приложения

Для запуска приложения перейдите в корен проекта и запустите 

```python entire.py```

По умолчанию приложение запускается на порту 5000, но вы можете кастомизировать port, например:
```py entire.py --port 8001```


## Тестовые сценарии

1) создание товара:
POST /product/create/ 
пример: ```{"name": "stuff", "params": {"weight": 1500, "height": 200}}```

2) поиск товара по...
названию:
POST /product/filter/ 
пример: ```{"name": "stuff"}```

параметрам:
POST /product/filter/ 
пример: ```{"params": {"height": 250}}```

3) получения товара по ID:
GET /product/get_by_id/{id}/ 
пример: ```http://127.0.0.1:8001/product/get_by_id/5d4ec686e5c8a1910cc1ade2/```

К сожалению не удалось победиь curl...
пытался отправить запрос 
```curl -d '{"name":"curl", "params":{"weight":1500,"height":200}}' -H "Content-Type: application/json" -X POST http://127.0.0.1:5000/product/create/``` но приложение ругается ```json.decoder.JSONDecodeError: Expecting value: line 1 column 1 (char 0)```
Хотя отправляя запрос postman'ом всё работает [скриншот postman](http://prntscr.com/ord7pb)
Пожалуйста, используйте postman для тестирования работоспособности приложения