import aiohttp
import argparse
from demo import create_app
from demo.settings import load_config
from motor.motor_asyncio import AsyncIOMotorClient


parser = argparse.ArgumentParser(description='test project')
parser.add_argument('--host', help="Host to listen", default='0.0.0.0') 
parser.add_argument('--port', help="Port to accept connections", default='5000') 
parser.add_argument(
    '--reload', 
    action="store_true", 
    help="Autoreload on change") 

parser.add_argument('-c', '--config', type=argparse.FileType('r'),
    help='path toConfig gile'
) 

args = parser.parse_args()

app = create_app(config=load_config(args.config))

if __name__ == '__main__':

    aiohttp.web.run_app(app, host=args.host, port=args.port)
